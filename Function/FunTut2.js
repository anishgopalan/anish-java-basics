let a = 10;
if (true)
{
    let b= 20; //let use for Local Variable 
    var c = 30;//var use for Global Variable
    console.log(a+b+c)
}
console.log(a+c)


const half1 = function(n )
{
    return n/2;
}
let n = 10;
console.log(half1(n));

const hummus = function(factor) {
    const ingredient = function(amount, unit, name) {
           let ingredientAmount = amount * factor;
    if (ingredientAmount > 1) {
    unit += "s";
    }
    console.log(`${ingredientAmount} ${unit} ${name}`);
    };
    ingredient(1, "can", "chickpeas");
    ingredient(0.25, "cup", "tahini");
    ingredient(0.25, "cup", "lemon juice");
    ingredient(1, "clove", "garlic");
    ingredient(2, "tablespoon", "olive oil");
    ingredient(0.5, "teaspoon", "cumin");
    };
    hummus()
//function 2nd method
   console.log("The future says",future())
     function future()
    {
        return "you will never (coming from future fun call)";
    }
//function 3rd method

const power =(base,exponent)=>
{
    let result =1;
    for (let count=0;count <exponent;count++){
        result *= base;
    }
return result;
}
console.log(power(5,3));
// function with one line of staement can be written like this
//const square = (x) =>
//{ return x*x} this can be written like below
const square = (x) => x*x;
console.log(square(8))
// BELOW FUNCTION RETURN A STACK OVER FLOW
/*
function chicken(){
    return egg();
}
function egg()
{
    return chicken();
}
console.log(chicken() + "WHICH CHICKEN")
*/
function square1(x)
{
return (x*x)
}
console.log(square1(4,true,"Anish Gop"));

function minus(a,b)
{
    if (b === undefined)
        return -a;
        else
        return a-b

}
console.log(minus(7));
console.log(minus(15,8));

function power1(base,exponent = 2)
{
    let result=1
    for (count = 0;count < exponent;count ++)
    {
        result *= base;
    }
return result
}
console.log(power1(5));
console.log(power1(4,5))
console.log("C","O",2);

function wrapWord(n)
{
let local = n;
return () => local;
}
let wrap1 = wrapWord(1);
let wrap2 = wrapWord(2);
console.log(wrap1());
console.log(wrap2());
