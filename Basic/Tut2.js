console.log(11 || 12)// or operator will return the value of left side if both true
console.log(0 || 12)// or operator will return the value if it is null or sero
console.log("user" || null)
console.log("Anish" || "user" )
console.log(null || "Anish" )
//#BINDING
let caught = 5*5;
console.log(caught)
let ten =10;
console.log(ten*ten)
let x = "Morning"
console.log(x)
x= "Evening"
console.log(x)

let debt =150
debt = debt-56
console.log(debt)

var name = "Anish";
const greeting = "Hello ";
console.log(greeting + name);
//s = prompt("Enter Password"); 
//document.write("Login in as " + s)
